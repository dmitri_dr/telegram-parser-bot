FROM alpine:latest
RUN apk update
RUN apk add py-pip 
RUN apk add python3-dev
RUN apk add gcc
RUN python3 -m pip install --upgrade pip
RUN pip install pyrogram

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install flask
RUN pip install aiogram

COPY . /app

ENTRYPOINT ["python3"]

CMD ["parserbot.py"]